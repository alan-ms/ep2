package br.unb.fga.oo.ep2.model;

public interface Combustivel {

    double ALCOOL = 3.499;
    double GASOLINA = 4.449;
    double DIESEL = 3.869;
}
