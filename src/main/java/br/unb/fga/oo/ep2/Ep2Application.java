package br.unb.fga.oo.ep2;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import br.unb.fga.oo.ep2.ui.Tela;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Ep2Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = new SpringApplicationBuilder(Ep2Application.class).headless(false).run(args);
	}
}
