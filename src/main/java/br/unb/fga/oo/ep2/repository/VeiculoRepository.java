package br.unb.fga.oo.ep2.repository;

import br.unb.fga.oo.ep2.model.Veiculo;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface VeiculoRepository extends VeiculoBaseRepository<Veiculo> {
}
