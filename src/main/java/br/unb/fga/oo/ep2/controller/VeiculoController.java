package br.unb.fga.oo.ep2.controller;

import br.unb.fga.oo.ep2.model.CarretaVan;
import br.unb.fga.oo.ep2.model.CarroMoto;
import br.unb.fga.oo.ep2.repository.CarretaVanRepository;
import br.unb.fga.oo.ep2.repository.CarroMotoRepository;
import br.unb.fga.oo.ep2.ui.Tela;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.util.List;

@Controller
public class VeiculoController {

    @Autowired
    private CarretaVanRepository carretaVanRepository;
    @Autowired
    private CarroMotoRepository carroMotoRepository;
    @Autowired
    private Tela tela;

    public VeiculoController() {
    }

    @PostConstruct
    public void init() {
        tela = new Tela(this);
        tela.setVisible(true);
    }

    public List<CarroMoto> findAllCarroMoto() {
        return carroMotoRepository.findAll();
    }

    public List<CarretaVan> findAllCarretaVan() {
        return carretaVanRepository.findAll();
    }

    //public

    public void createCM(CarroMoto cm) {
        carroMotoRepository.save(cm);
    }

    public void createCV(CarretaVan cv) {
        carretaVanRepository.save(cv);
    }

    public void deleteCM(Long id) {
        carroMotoRepository.deleteById(id);
    }

    public void deleteCV(Long id) {
        carretaVanRepository.deleteById(id);
    }

}
