package br.unb.fga.oo.ep2.repository;

import br.unb.fga.oo.ep2.model.Veiculo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface VeiculoBaseRepository <T extends Veiculo>  extends JpaRepository<T, Long> {
}
