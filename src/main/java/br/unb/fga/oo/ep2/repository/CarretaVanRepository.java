package br.unb.fga.oo.ep2.repository;

import br.unb.fga.oo.ep2.model.CarretaVan;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface CarretaVanRepository extends VeiculoBaseRepository<CarretaVan> {
}
