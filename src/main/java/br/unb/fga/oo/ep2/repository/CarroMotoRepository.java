package br.unb.fga.oo.ep2.repository;

import br.unb.fga.oo.ep2.model.CarroMoto;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface CarroMotoRepository extends VeiculoBaseRepository<CarroMoto> {
}
