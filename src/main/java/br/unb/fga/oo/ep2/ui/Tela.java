package br.unb.fga.oo.ep2.ui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import br.unb.fga.oo.ep2.controller.VeiculoController;
import br.unb.fga.oo.ep2.model.CarretaVan;
import br.unb.fga.oo.ep2.model.CarroMoto;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.text.DecimalFormat;
import java.util.List;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@Component
public class Tela extends JFrame {

	private JPanel mainPane;
	private JTextField textModelo;
	private JTextField textCargaMaxima;
	private JTextField textVelodidadeMedia;
	private JTextField textEsgotamentoAlcool;
	private JTextField textEsgotamentoGasolina;
	private JTextField textRendimentoDiesel;
	private JTextField textEsgotamentoDiesel;
	private JTextField textRendimentoAlcool;
	private JTextField textRendimentoGasolina;
	private JTable tableVeiculo;
	private String[][] veiculos;
	private VeiculoController veiculoController;
	private JComboBox listTipoVeiculo;
	private JComboBox listDisponivel;
	private JLabel lblID;
	private String[] colunaTabela = { "ID", "MODELO", "CARGA MÁXIMA", "VELOCIDADE MÉDIA", "TIPO DE VEÍCULO", "DISPONÍVEL",
			"RENDIMENTO GASOLINA", "ESGOTAMENTO GASOLINA", "REDIMENTO ÁLCOOL", "ESGOTAMENTO ALCOOL",
			"RENDIMENTO DIESEL", "ESGOTAMENTO DIESEL" };
	private JTextField textPeso;

	public Tela(VeiculoController veiculoController) {
		this.veiculoController = veiculoController;
		setResizable(false);
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1058, 674);
		mainPane = new JPanel();
		mainPane.setBackground(Color.WHITE);
		mainPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(mainPane);
		mainPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(6, 0, 1046, 638);
		mainPane.add(tabbedPane);

		JPanel veiculoPanel = new JPanel();
		tabbedPane.addTab("Veículos", null, veiculoPanel, null);
		veiculoPanel.setBackground(Color.WHITE);
		veiculoPanel.setLayout(null);

		JLabel lblModelo = new JLabel("Modelo:");
		lblModelo.setBounds(6, 13, 66, 15);
		veiculoPanel.add(lblModelo);

		JLabel lblCargaMaxima = new JLabel("Carga Máxima:");
		lblCargaMaxima.setBounds(199, 13, 113, 15);
		veiculoPanel.add(lblCargaMaxima);

		textModelo = new JTextField();
		textModelo.setBounds(66, 8, 98, 25);
		veiculoPanel.add(textModelo);
		textModelo.setColumns(10);

		textCargaMaxima = new JTextField();
		textCargaMaxima.setBounds(309, 8, 80, 25);
		veiculoPanel.add(textCargaMaxima);
		textCargaMaxima.setColumns(10);

		JLabel lblKg = new JLabel("Kg");
		lblKg.setBounds(395, 13, 24, 15);
		veiculoPanel.add(lblKg);

		listTipoVeiculo = new JComboBox();
		listTipoVeiculo.setBounds(904, 8, 124, 25);
		veiculoPanel.add(listTipoVeiculo);

		JLabel lblTipoVeculo = new JLabel("Tipo de Veículo:");
		lblTipoVeculo.setBounds(787, 13, 129, 15);
		veiculoPanel.add(lblTipoVeculo);

		JLabel lblNewLabel = new JLabel("Velocidade Média:");
		lblNewLabel.setBounds(527, 12, 163, 23);
		veiculoPanel.add(lblNewLabel);

		textVelodidadeMedia = new JTextField();
		textVelodidadeMedia.setBounds(654, 8, 80, 25);
		veiculoPanel.add(textVelodidadeMedia);
		textVelodidadeMedia.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Rendimento Álcool:");
		lblNewLabel_1.setBounds(163, 111, 134, 15);
		veiculoPanel.add(lblNewLabel_1);

		textEsgotamentoAlcool = new JTextField();
		textEsgotamentoAlcool.setBounds(654, 105, 80, 27);
		veiculoPanel.add(textEsgotamentoAlcool);
		textEsgotamentoAlcool.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Redimento Gasolina:");
		lblNewLabel_2.setBounds(158, 61, 141, 15);
		veiculoPanel.add(lblNewLabel_2);

		textEsgotamentoGasolina = new JTextField();
		textEsgotamentoGasolina.setBounds(654, 56, 80, 25);
		veiculoPanel.add(textEsgotamentoGasolina);
		textEsgotamentoGasolina.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("Esgotamento Álcool:");
		lblNewLabel_3.setBounds(499, 111, 178, 15);
		veiculoPanel.add(lblNewLabel_3);

		textRendimentoDiesel = new JTextField();
		textRendimentoDiesel.setBounds(309, 154, 80, 25);
		veiculoPanel.add(textRendimentoDiesel);
		textRendimentoDiesel.setColumns(10);

		JLabel lblNewLabel_4 = new JLabel("Esgotamento Diesel:");
		lblNewLabel_4.setBounds(499, 159, 141, 15);
		veiculoPanel.add(lblNewLabel_4);

		textEsgotamentoDiesel = new JTextField();
		textEsgotamentoDiesel.setBounds(654, 153, 80, 27);
		veiculoPanel.add(textEsgotamentoDiesel);
		textEsgotamentoDiesel.setColumns(10);

		JLabel lblNewLabel_5 = new JLabel("Redimento Diesel:");
		lblNewLabel_5.setBounds(173, 159, 124, 15);
		veiculoPanel.add(lblNewLabel_5);

		textRendimentoAlcool = new JTextField();
		textRendimentoAlcool.setBounds(309, 105, 80, 27);
		veiculoPanel.add(textRendimentoAlcool);
		textRendimentoAlcool.setColumns(10);

		JLabel lblNewLabel_6 = new JLabel("Esgotamento Gasolina:");
		lblNewLabel_6.setBounds(485, 61, 157, 15);
		veiculoPanel.add(lblNewLabel_6);

		textRendimentoGasolina = new JTextField();
		textRendimentoGasolina.setBounds(309, 56, 80, 25);
		veiculoPanel.add(textRendimentoGasolina);
		textRendimentoGasolina.setColumns(10);

		JLabel lblNewLabel_7 = new JLabel("Km/L");
		lblNewLabel_7.setBounds(395, 61, 39, 15);
		veiculoPanel.add(lblNewLabel_7);

		JLabel label = new JLabel("Km/L");
		label.setBounds(740, 111, 58, 15);
		veiculoPanel.add(label);

		JLabel label_1 = new JLabel("Km/L");
		label_1.setBounds(396, 111, 58, 15);
		veiculoPanel.add(label_1);

		JLabel label_2 = new JLabel("Km/L");
		label_2.setBounds(740, 61, 58, 15);
		veiculoPanel.add(label_2);

		JLabel label_3 = new JLabel("Km/L");
		label_3.setBounds(395, 159, 58, 15);
		veiculoPanel.add(label_3);

		JLabel label_4 = new JLabel("Km/L");
		label_4.setBounds(740, 159, 58, 15);
		veiculoPanel.add(label_4);

		JLabel lblKmh = new JLabel("Km/h");
		lblKmh.setBounds(740, 13, 58, 15);
		veiculoPanel.add(lblKmh);

		JButton btnCriar = new JButton("Criar Veículo");
		btnCriar.addActionListener(arg0 -> {
			try {
				if (!textModelo.getText().equals("") && !textCargaMaxima.getText().equals("")
						&& !textVelodidadeMedia.equals("")) {
					if (listTipoVeiculo.getSelectedItem().toString().equals("Carro")
							|| listTipoVeiculo.getSelectedItem().toString().equals("Moto")) {
						CarroMoto cm = getInputCarroMoto();
						cm.setDisponivel("sim");
						veiculoController.createCM(cm);
					} else if (listTipoVeiculo.getSelectedItem().toString().equals("Carreta")
							|| listTipoVeiculo.getSelectedItem().toString().equals("Van")) {
						CarretaVan cv = getInputCarretaVan();
						cv.setDisponivel("sim");
						veiculoController.createCV(cv);
					}
				}
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Algum erro inesperado aconteceu!", "ERRO!",
						JOptionPane.ERROR_MESSAGE);
			}
			recarregar();
			limparCampos();
		});
		btnCriar.setBounds(163, 256, 134, 27);
		veiculoPanel.add(btnCriar);

		JButton btnDelete = new JButton("Deletar Veículo");
		btnDelete.addActionListener(arg0 -> {
			if (!lblID.getText().equals("-")) {
				if (listTipoVeiculo.getSelectedItem().toString().equals("Carro")
						|| listTipoVeiculo.getSelectedItem().toString().equals("Moto")) {
					veiculoController.deleteCM(Long.parseLong(lblID.getText()));
				} else if (listTipoVeiculo.getSelectedItem().toString().equals("Carreta")
						|| listTipoVeiculo.getSelectedItem().toString().equals("Van")) {
					veiculoController.deleteCV(Long.parseLong(lblID.getText()));
				}
			}
			recarregar();
			limparCampos();
		});
		btnDelete.setBounds(517, 256, 149, 27);
		veiculoPanel.add(btnDelete);

		JButton btnLimpa = new JButton("Limpa Campos");
		btnLimpa.addActionListener(arg0 -> limparCampos());
		btnLimpa.setBounds(708, 256, 134, 27);
		veiculoPanel.add(btnLimpa);

		veiculos = listarVeiculos();
		DefaultTableModel tableModel = new DefaultTableModel(veiculos, colunaTabela);

		String[] tipo_veiculo = { "Selecionar", "Carro", "Moto", "Carreta", "Van" };
		for (String tv : tipo_veiculo) {
			listTipoVeiculo.addItem(tv);
		}
		listTipoVeiculo.setSelectedIndex(0);

		listDisponivel = new JComboBox();
		listDisponivel.setBounds(648, 199, 124, 25);

		String[] disponivel = {"sim", "nao"};
		for (String d: disponivel) {
			listDisponivel.addItem(d);
		}
		veiculoPanel.add(listDisponivel);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(6, 295, 1034, 314);
		veiculoPanel.add(scrollPane);
		tableVeiculo = new JTable(tableModel);
		tableVeiculo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				limparCampos();
				int row = tableVeiculo.getSelectedRow();
				lblID.setText(tableVeiculo.getValueAt(row, 0).toString());
				textModelo.setText(tableVeiculo.getValueAt(row, 1).toString());
				textCargaMaxima.setText(tableVeiculo.getValueAt(row, 2).toString());
				textVelodidadeMedia.setText(tableVeiculo.getValueAt(row, 3).toString());
				if (tableVeiculo.getValueAt(row, 4).toString().equals("Carro") || tableVeiculo.getValueAt(row, 4).toString().equals("Moto")) {
					textRendimentoGasolina.setText(tableVeiculo.getValueAt(row, 6).toString());
					textEsgotamentoGasolina.setText(tableVeiculo.getValueAt(row, 7).toString());
					textRendimentoAlcool.setText(tableVeiculo.getValueAt(row, 8).toString());
					textEsgotamentoAlcool.setText(tableVeiculo.getValueAt(row, 9).toString());
				} else if (tableVeiculo.getValueAt(row, 4).toString().equals("Carreta") || tableVeiculo.getValueAt(row, 4).toString().equals("Van")){
					textEsgotamentoDiesel.setText(tableVeiculo.getValueAt(row, 11).toString());
					textRendimentoDiesel.setText(tableVeiculo.getValueAt(row, 10).toString());
				}
				switch (tableVeiculo.getValueAt(row, 4).toString()) {
					case "Carro":
						listTipoVeiculo.setSelectedIndex(1);
						break;
					case "Moto":
						listTipoVeiculo.setSelectedIndex(2);
						break;
					case "Carreta":
						listTipoVeiculo.setSelectedIndex(3);
						break;
					case "Van":
						listTipoVeiculo.setSelectedIndex(4);
						break;
					default:
						listTipoVeiculo.setSelectedIndex(0);
				}
				if (tableVeiculo.getValueAt(row, 5).toString().equals("sim")) {
					listDisponivel.setSelectedIndex(0);
				} else {
					listDisponivel.setSelectedIndex(1);
				}
			}
		});
		tableVeiculo.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableVeiculo.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		scrollPane.setViewportView(tableVeiculo);

		JButton btnAlterarVeculo = new JButton("Alterar Veículo");
		btnAlterarVeculo.addActionListener(arg0 -> {
			try {
				if (!textModelo.getText().equals("") && !textCargaMaxima.getText().equals("")
						&& !textVelodidadeMedia.equals("")) {
					if (listTipoVeiculo.getSelectedItem().toString().equals("Carro")
							|| listTipoVeiculo.getSelectedItem().toString().equals("Moto")) {
						CarroMoto cm = getInputCarroMoto();
						cm.setId(Long.parseLong(lblID.getText()));
						cm.setDisponivel(listDisponivel.getSelectedItem().toString());
						veiculoController.createCM(cm);
					} else if (listTipoVeiculo.getSelectedItem().toString().equals("Carreta")
							|| listTipoVeiculo.getSelectedItem().toString().equals("Van")) {
						CarretaVan cv = getInputCarretaVan();
						cv.setId(Long.parseLong(lblID.getText()));
						cv.setDisponivel(listDisponivel.getSelectedItem().toString());
						veiculoController.createCV(cv);
					}
				}
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Algum erro inesperado aconteceu!", "ERRO!",
						JOptionPane.ERROR_MESSAGE);
			}
			recarregar();
			limparCampos();
		});
		btnAlterarVeculo.setBounds(341, 256, 134, 27);
		veiculoPanel.add(btnAlterarVeculo);

		JLabel lblDisponvel = new JLabel("Disponível:");
		lblDisponvel.setBounds(553, 204, 113, 15);
		veiculoPanel.add(lblDisponvel);

		lblID = new JLabel("-");
		lblID.setBounds(309, 209, 58, 15);
		veiculoPanel.add(lblID);

		JLabel lblID = new JLabel("Id:");
		lblID.setBounds(254, 209, 58, 15);
		veiculoPanel.add(lblID);

		listTipoVeiculo.addItemListener(item -> {
			if (item.getItem().toString().equals(tipo_veiculo[1])
					|| item.getItem().toString().equals(tipo_veiculo[2])) {
				textRendimentoDiesel.disable();
				textEsgotamentoDiesel.disable();
				textRendimentoAlcool.enable();
				textEsgotamentoAlcool.enable();
				textRendimentoGasolina.enable();
				textEsgotamentoGasolina.enable();

			} else if (item.getItem().toString().equals(tipo_veiculo[3])
					|| item.getItem().toString().equals(tipo_veiculo[4])) {
				textRendimentoDiesel.enable();
				textEsgotamentoDiesel.enable();
				textRendimentoAlcool.disable();
				textEsgotamentoAlcool.disable();
				textRendimentoGasolina.disable();
				textEsgotamentoGasolina.disable();
			} else {
				textRendimentoAlcool.disable();
				textEsgotamentoAlcool.disable();
				textRendimentoGasolina.disable();
				textEsgotamentoGasolina.disable();
				textRendimentoDiesel.disable();
				textEsgotamentoDiesel.disable();
			}
		});

		JPanel entregaPanel = new JPanel();
		tabbedPane.addTab("Calcular Entrega", null, entregaPanel, null);
		entregaPanel.setLayout(null);
		
		JLabel lblPrecoGasolina = new JLabel("GASOLINA: R$ 4.449 por litro");
		lblPrecoGasolina.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
		lblPrecoGasolina.setBounds(17, 29, 208, 21);
		entregaPanel.add(lblPrecoGasolina);
		
		JLabel lblNewLabel_8 = new JLabel("ÁLCOOL: R$ 3.499 por litro");
		lblNewLabel_8.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
		lblNewLabel_8.setBounds(17, 62, 182, 15);
		entregaPanel.add(lblNewLabel_8);
		
		JLabel lblNewLabel_9 = new JLabel("DISEL: R$ 3.869 por litro");
		lblNewLabel_9.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
		lblNewLabel_9.setBounds(17, 95, 208, 15);
		entregaPanel.add(lblNewLabel_9);
		
		JLabel lblNewLabel_10 = new JLabel("Peso da Carga:");
		lblNewLabel_10.setBounds(549, 62, 115, 15);
		entregaPanel.add(lblNewLabel_10);
		
		JLabel lblNewLabel_11 = new JLabel("Distância:");
		lblNewLabel_11.setBounds(582, 95, 91, 15);
		entregaPanel.add(lblNewLabel_11);
		
		JLabel lblNewLabel_12 = new JLabel("Margem de lucro:");
		lblNewLabel_12.setBounds(531, 131, 158, 15);
		entregaPanel.add(lblNewLabel_12);
		
		JSpinner spinnerLucro = new JSpinner();
		spinnerLucro.setBounds(676, 126, 132, 26);
		entregaPanel.add(spinnerLucro);
		
		textPeso = new JTextField();
		textPeso.setHorizontalAlignment(SwingConstants.RIGHT);
		textPeso.setBounds(676, 56, 132, 27);
		entregaPanel.add(textPeso);
		textPeso.setColumns(10);
		
		JSpinner spinnerDistancia = new JSpinner();
		spinnerDistancia.setModel(new SpinnerNumberModel(new Double(1), new Double(1), null, new Double(4)));
		spinnerDistancia.setBounds(675, 89, 133, 26);
		entregaPanel.add(spinnerDistancia);
		
		JLabel lblNewLabel_13 = new JLabel("CÁCULOS DE ENTREGA");
		lblNewLabel_13.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
		lblNewLabel_13.setBounds(560, 16, 190, 15);
		entregaPanel.add(lblNewLabel_13);
		JLabel lblMenorTempo = new JLabel("-");
		lblMenorTempo.setBounds(412, 324, 605, 15);
		entregaPanel.add(lblMenorTempo);

		JLabel lblCustoBeneficio = new JLabel("-");
		lblCustoBeneficio.setBounds(412, 376, 605, 15);
		entregaPanel.add(lblCustoBeneficio);
		
		JLabel lblCustoOperacao = new JLabel("-");
		lblCustoOperacao.setBounds(412, 278, 605, 15);
		entregaPanel.add(lblCustoOperacao);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(arg0 -> {
			DecimalFormat df = new DecimalFormat("#####.##");

			double distancia  = Double.parseDouble(spinnerDistancia.getValue().toString());
			double carga = Double.parseDouble(textPeso.getText());
			double margemLucro = 1 + (Double.parseDouble(spinnerLucro.getValue().toString()) / 100);

			List<CarroMoto> carroMotos = veiculoController.findAllCarroMoto();
			List<CarretaVan> carretaVans = veiculoController.findAllCarretaVan();
			CarroMoto carroMoto = new CarroMoto();
			CarretaVan carretaVan = new CarretaVan();

			double[][] menorCustoOperacaoCarroMoto = carroMoto.menorCustoOperacao(carga, distancia, carroMotos);
			double[][] menorCustoOperacaoCarretaVan = carretaVan.menorCustoOperacao(carga, distancia, carretaVans);
			double[][] menorTempoCarroMoto = carroMoto.menorTempo(distancia, carga, carroMotos);
			double[][] menorTempoCarretaVan = carretaVan.menorTempo(carga, distancia, carretaVans);
			double[][] custoOperacaoOrdenadoCarroMoto = carroMoto.ordenar(menorCustoOperacaoCarroMoto, 1);
			double[][] custoOperacaoOrdenadoCarretaVan = carroMoto.ordenar(menorCustoOperacaoCarretaVan, 1);
			double[][] tempoOrdenadoCarroMoto = carroMoto.ordenar(menorTempoCarroMoto, 1);
			double[][] tempoOrdenadoCarretaVan = carroMoto.ordenar(menorTempoCarroMoto, 1);
			double[][] custoBeneficioCarroMoto = carroMoto.custoBeneficio(menorCustoOperacaoCarroMoto, menorTempoCarroMoto);
			double[][] custoBeneficioCarrevaVan = carretaVan.custoBeneficio(menorCustoOperacaoCarretaVan, menorTempoCarretaVan);


			if (menorCustoOperacaoCarretaVan == null || menorCustoOperacaoCarroMoto[0][1] < menorCustoOperacaoCarretaVan[0][1]) {
				lblCustoOperacao.setText("ID: " + (int) menorCustoOperacaoCarroMoto[0][0] +
						" Custo de Operação: R$" + df.format(menorCustoOperacaoCarroMoto[0][1]) +
						" Tempo de viagem: " + df.format(tempoOrdenadoCarroMoto[(int)menorCustoOperacaoCarroMoto[0][0]][1]) + " h" +
						" Valor total Viagem: R$ " + (menorCustoOperacaoCarroMoto[0][0] * margemLucro));
			} else if (menorCustoOperacaoCarroMoto == null) {
				lblCustoOperacao.setText("ID: " + (int) menorCustoOperacaoCarretaVan[0][0] +
						" Custo de Operação: R$" + df.format(menorCustoOperacaoCarretaVan[0][1]) +
						" Tempo de viagem: " + df.format(tempoOrdenadoCarroMoto[(int)menorCustoOperacaoCarretaVan[0][0]][1]) + " h" +
						" Valor total Viagem: R$ " + (menorCustoOperacaoCarroMoto[0][0] * margemLucro));
			} else {
				JOptionPane.showMessageDialog(null, "Algum erro aconteceu!", "ERROR", JOptionPane.ERROR_MESSAGE);
			}

			if (menorTempoCarretaVan == null || menorTempoCarroMoto[0][1] < menorTempoCarretaVan[0][1]) {
				lblMenorTempo.setText("ID: " + (int) menorTempoCarroMoto[0][0] +
						" Tempo de viagem: " + df.format(menorTempoCarroMoto[0][1])+ " h" +
						" Valor total Viagem: R$ " + (custoOperacaoOrdenadoCarroMoto[(int) menorTempoCarroMoto[0][0]][0] * margemLucro));
			} else if (menorTempoCarroMoto == null) {
				lblMenorTempo.setText("ID: " + (int) menorTempoCarretaVan[0][0] +
						" Tempo de viagem: " + df.format(tempoOrdenadoCarretaVan[0][1]) + " h" +
						" Valor total Viagem: R$ " + (custoOperacaoOrdenadoCarretaVan[(int)menorCustoOperacaoCarretaVan[0][0]][0] * margemLucro));
			} else {
				JOptionPane.showMessageDialog(null, "Algum erro aconteceu!", "ERROR", JOptionPane.ERROR_MESSAGE);
			}
		
		});
		btnCalcular.setFont(new Font("DejaVu Sans", Font.BOLD, 14));
		btnCalcular.setBounds(686, 172, 99, 27);
		entregaPanel.add(btnCalcular);
		
		JLabel lblNewLabel_17 = new JLabel("Km");
		lblNewLabel_17.setBounds(820, 95, 58, 15);
		entregaPanel.add(lblNewLabel_17);
		
		JLabel lblNewLabel_18 = new JLabel("%");
		lblNewLabel_18.setBounds(820, 132, 58, 15);
		entregaPanel.add(lblNewLabel_18);
		
		JLabel lblKg_1 = new JLabel("Kg");
		lblKg_1.setBounds(820, 62, 58, 15);
		entregaPanel.add(lblKg_1);
		
		JButton btnCustoOperacao = new JButton("Menor custo de operação");
		btnCustoOperacao.setBounds(152, 273, 226, 25);
		entregaPanel.add(btnCustoOperacao);
		
		JButton btnMenorTempoViajem = new JButton("Menor tempo viajem");
		btnMenorTempoViajem.setBounds(152, 319, 226, 25);
		entregaPanel.add(btnMenorTempoViajem);
		
		JButton btnNCustoBeneficio = new JButton("Melhor custo benefício");
		btnNCustoBeneficio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNCustoBeneficio.setBounds(152, 371, 226, 25);
		entregaPanel.add(btnNCustoBeneficio);
	}

	public String[][] listarVeiculos() {
		List<CarroMoto> carromMotos = veiculoController.findAllCarroMoto();
		List<CarretaVan> carretaVan = veiculoController.findAllCarretaVan();
		String[][] veiculos = new String[carromMotos.size() + carretaVan.size()][12];
		int i = 0;
		for (int j = 0; j < carromMotos.size(); j++) {
			veiculos[i][0] = carromMotos.get(j).getId().toString();
			veiculos[i][1] = carromMotos.get(j).getModelo();
			veiculos[i][2] = Double.toString(carromMotos.get(j).getCargaMaxima());
			veiculos[i][3] = Double.toString(carromMotos.get(j).getVelocidadeMedia());
			veiculos[i][4] = carromMotos.get(j).getVeiculo();
			veiculos[i][5] = carromMotos.get(j).getDisponivel();
			veiculos[i][6] = Double.toString(carromMotos.get(j).getRendimentoGasolina());
			veiculos[i][7] = Double.toString(carromMotos.get(j).getEsgotamentoGasolina());
			veiculos[i][8] = Double.toString(carromMotos.get(j).getRendimentoAlcool());
			veiculos[i][9] = Double.toString(carromMotos.get(j).getEsgotamentoAlcool());
			i++;
		}
		for (int j = 0; j < carretaVan.size(); j++) {
			veiculos[i][0] = carretaVan.get(j).getId().toString();
			veiculos[i][1] = carretaVan.get(j).getModelo();
			veiculos[i][2] = Double.toString(carretaVan.get(j).getCargaMaxima());
			veiculos[i][3] = Double.toString(carretaVan.get(j).getVelocidadeMedia());
			veiculos[i][4] = carretaVan.get(j).getVeiculo();
			veiculos[i][5] = carretaVan.get(j).getDisponivel();
			veiculos[i][10] = Double.toString(carretaVan.get(j).getRendimentoDiesel());
			veiculos[i][11] = Double.toString(carretaVan.get(j).getEsgotamentoDiesel());
			i++;
		}
		return veiculos;
	}

	private void recarregar() {
		String[][] veiculos = listarVeiculos();
		DefaultTableModel dt = new DefaultTableModel(veiculos, colunaTabela);
		tableVeiculo.setModel(dt);
	}

	private void limparCampos() {
		lblID.setText("");
		textModelo.setText("");
		textCargaMaxima.setText("");
		textVelodidadeMedia.setText("");
		textEsgotamentoAlcool.setText("");
		textRendimentoAlcool.setText("");
		textEsgotamentoGasolina.setText("");
		textRendimentoGasolina.setText("");
		textEsgotamentoDiesel.setText("");
		textRendimentoDiesel.setText("");
		listTipoVeiculo.setSelectedIndex(0);
	}

	private CarroMoto getInputCarroMoto() {
		CarroMoto carroMoto;
		if (!textRendimentoGasolina.getText().equals("")
				&& !textEsgotamentoGasolina.getText().equals("")
				&& !textRendimentoAlcool.getText().equals("")
				&& !textEsgotamentoAlcool.getText().equals("")) {
			carroMoto = new CarroMoto();
			carroMoto.setModelo(textModelo.getText());
			carroMoto.setCargaMaxima(Double.parseDouble(textCargaMaxima.getText()));
			carroMoto.setVelocidadeMedia(Double.parseDouble(textVelodidadeMedia.getText()));
			carroMoto.setEsgotamentoAlcool(Double.parseDouble(textEsgotamentoAlcool.getText()));
			carroMoto.setRendimentoAlcool(Double.parseDouble(textRendimentoAlcool.getText()));
			carroMoto.setEsgotamentoGasolina(Double.parseDouble(textEsgotamentoGasolina.getText()));
			carroMoto.setRendimentoGasolina(Double.parseDouble(textRendimentoGasolina.getText()));
			if (listTipoVeiculo.getSelectedItem().toString().equals("Carro")) {
				carroMoto.setVeiculo("Carro");
			} else if (listTipoVeiculo.getSelectedItem().toString().equals("Moto")) {
				carroMoto.setVeiculo("Moto");
			}
			carroMoto.setDisponivel("sim");
		} else {
			JOptionPane.showMessageDialog(null, "Algum campo sem valor!", null,
					JOptionPane.INFORMATION_MESSAGE);
			return null;
		}
		return carroMoto;
	}

	private CarretaVan getInputCarretaVan() {
		CarretaVan carretaVan = new CarretaVan();
		if (!textRendimentoDiesel.getText().equals("")
				&& !textEsgotamentoDiesel.getText().equals("")) {
			carretaVan = new CarretaVan();
			carretaVan.setModelo(textModelo.getText());
			carretaVan.setCargaMaxima(Double.parseDouble(textCargaMaxima.getText()));
			carretaVan.setVelocidadeMedia(Double.parseDouble(textVelodidadeMedia.getText()));
			carretaVan.setRendimentoDiesel(Double.parseDouble(textRendimentoDiesel.getText()));
			carretaVan.setEsgotamentoDiesel(Double.parseDouble(textEsgotamentoDiesel.getText()));
			carretaVan.setDisponivel(listDisponivel.getSelectedItem().toString());
			if (listTipoVeiculo.getSelectedItem().toString().equals("Carreta")) {
				carretaVan.setVeiculo("Carreta");
			} else if (listTipoVeiculo.getSelectedItem().toString().equals("Van")) {
				carretaVan.setVeiculo("Van");
			}
		} else {
			JOptionPane.showMessageDialog(null, "Campo sem valor!", null,
					JOptionPane.INFORMATION_MESSAGE);
		}
		return carretaVan;
	}
}
