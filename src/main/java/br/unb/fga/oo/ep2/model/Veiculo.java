package br.unb.fga.oo.ep2.model;


import javax.persistence.*;

@Entity
@Inheritance
@DiscriminatorColumn(name = "tipo_veiculo")
public class Veiculo implements Combustivel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "modelo")
    private String modelo;
    @Column(name = "carga_maxima")
    private double cargaMaxima;
    @Column(name = "velocidade_media")
    private double velocidadeMedia;
    @Column(name = "veiculo")
    private String veiculo;
    @Column(name = "disponivel")
    private String disponivel;

    public Veiculo() {}

    public double[][] ordenar(double[][] veiculos, int opcao) {
        if (veiculos == null) {
            return null;
        }
        if (veiculos.length > 1) {
            int i, j;
            double[] aux;
            for (i = 1; i < veiculos.length; i++) {
//                aux[0] = veiculos[i][0];
//                aux[1] = veiculos[i][1];
                aux = veiculos[i];
                j = i;
                if (opcao == 0) {
                    while ((j > 0) && aux[1] < veiculos[j - 1][1]) {
//                        veiculos[j][0] = veiculos[j - 1][0];
//                        veiculos[j][1] = veiculos[j - 1][1];
                        veiculos[j] = veiculos[j - 1];
                        j -= 1;
                    }
                } else {
                    while ((j > 0) && aux[0] < veiculos[j - 1][0]) {
//                        veiculos[j][0] = veiculos[j - 1][0];
//                        veiculos[j][1] = veiculos[j - 1][1];
                        veiculos[j] = veiculos[j - 1];
                        j -= 1;
                    }
                }
//                veiculos[j][0] = aux[0];
//                veiculos[j][1] = aux[1];
                veiculos[j] = aux;
            }
        }
        return veiculos;
    }

    public double[][] custoBeneficio(double[][] custoOperacao, double[][] tempo) {
        if (custoOperacao == null) {
            return null;
        }
        custoOperacao = ordenar(custoOperacao, 1);
        tempo = ordenar(tempo, 1);
        double[][] custoBeneficio = new double[custoOperacao.length][2];
        for (int i = 0; i < custoOperacao.length; i++) {
            custoBeneficio[i][0] = custoOperacao[i][0];
            for (int j = 0; j < tempo.length; j++) {
                if (tempo[j][0] == custoOperacao[i][0]) {
                    custoBeneficio[i][1] = tempo[j][1];
                    break;
                }
            }
        }
        return ordenar(custoBeneficio, 0);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModelo() {
        return modelo;
    }

    public double getCargaMaxima() {
        return cargaMaxima;
    }

    public double getVelocidadeMedia() {
        return velocidadeMedia;
    }

    public String getVeiculo() {
        return veiculo;
    }

    public String getDisponivel() {
        return disponivel;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setCargaMaxima(double cargaMaxima) {
        this.cargaMaxima = cargaMaxima;
    }

    public void setVelocidadeMedia(double velocidadeMedia) {
        this.velocidadeMedia = velocidadeMedia;
    }

    public void setVeiculo(String veiculo) {
        this.veiculo = veiculo;
    }

    public void setDisponivel(String disponivel) {
        this.disponivel = disponivel;
    }

}
