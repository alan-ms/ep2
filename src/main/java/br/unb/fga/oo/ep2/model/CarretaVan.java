package br.unb.fga.oo.ep2.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

@Entity
@DiscriminatorValue("carreta_van")
public class CarretaVan extends Veiculo {

    @Column(name = "redimento_diesel")
    private double rendimentoDiesel;
    @Column(name = "esgotamento_diesel")
    private double esgotamentoDiesel;

    public CarretaVan() {}

    public double[][] menorCustoOperacao(double carga, double distancia, List<CarretaVan> carretaVans) {
        if (carretaVans.isEmpty()) {
            return null;
        }
        int z = 0;
        double[][] veiculos = new double[carretaVans.size()][2];
        for (int i = 0; i < carretaVans.size(); i++) {
            if (carga <= carretaVans.get(i).getCargaMaxima()) {
                veiculos[z][0] = carretaVans.get(i).getId();
                veiculos[z][1] = Combustivel.GASOLINA * (distancia / (carretaVans.get(i).getRendimentoDiesel() - (carga * carretaVans.get(i).getEsgotamentoDiesel())));
                z++;
            }
        }
        return ordenar(veiculos, 0);
    }

    public double[][] menorTempo(double carga, double distancia, List<CarretaVan> carretaVans) {
        if (carretaVans.isEmpty()) {
            return null;
        }
        int z = 0;
        double[][] veiculos = new double[carretaVans.size() * 2][2];
        for (int i = 0; i < carretaVans.size(); i++) {
            if (carga <= carretaVans.get(i).getCargaMaxima()) {
                veiculos[z][0] = carretaVans.get(i).getId();
                veiculos[z][1] = distancia / carretaVans.get(i).getVelocidadeMedia();
                z++;
            }
        }
        return ordenar(veiculos, 0);
    }

    public double getRendimentoDiesel() {
        return rendimentoDiesel;
    }

    public void setRendimentoDiesel(double rendimentoDiesel) {
        this.rendimentoDiesel = rendimentoDiesel;
    }

    public double getEsgotamentoDiesel() {
        return esgotamentoDiesel;
    }

    public void setEsgotamentoDiesel(double esgotamentoDiesel) {
        this.esgotamentoDiesel = esgotamentoDiesel;
    }
}
