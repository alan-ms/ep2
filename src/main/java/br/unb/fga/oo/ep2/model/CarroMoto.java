package br.unb.fga.oo.ep2.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import java.util.List;

@Entity
@DiscriminatorColumn(name = "carro_moto")
public class CarroMoto extends Veiculo {

    @Column(name = "redimento_gasolina")
    private double rendimentoGasolina;
    @Column(name = "rendimento_alcool")
    private double rendimentoAlcool;
    @Column(name = "esgotamento_gasolina")
    private double esgotamentoGasolina;
    @Column(name = "esgotamento_alcool")
    private double esgotamentoAlcool;

    public CarroMoto() {}

    public double getRendimentoGasolina() {
        return rendimentoGasolina;
    }

    public double[][] menorCustoOperacao(double carga, double distancia, List<CarroMoto> carroMotos) {
        if (carroMotos.isEmpty()) {
            return null;
        }
        int z = 0;
        double[][] veiculos = new double[carroMotos.size() * 2][3];
        for (int i = 0; i < carroMotos.size(); i++) {
            if (carga <= carroMotos.get(i).getCargaMaxima()) {
                veiculos[z][0] = carroMotos.get(i).getId();
                veiculos[z][1] = Combustivel.GASOLINA * (distancia / (carroMotos.get(i).getRendimentoGasolina() - (carga * carroMotos.get(i).getEsgotamentoGasolina())));
                veiculos[z][2] = 1; // IDENTIFICAR O TIPO DE COMBUSTIVEL - GASOLINA
                z++;
            }
        }
        for (int i = 0; i < carroMotos.size(); i++) {
            if (carga <= carroMotos.get(i).getCargaMaxima()) {
                veiculos[z][0] = carroMotos.get(i).getId();
                veiculos[z][1] = Combustivel.ALCOOL * (distancia / (carroMotos.get(i).getRendimentoAlcool() - (carga * carroMotos.get(i).getEsgotamentoAlcool())));
                veiculos[z][2] = 1; // IDENTIFICAR O TIPO DE COMBUSTIVEL - ÁLCOOL
                z++;
            }
        }
        return ordenar(veiculos, 0);
    }

    public double[][] menorTempo(double carga, double distancia, List<CarroMoto> carroMotos) {
        if (carroMotos.isEmpty()) {
            return null;
        }
        int z = 0;
        double[][] veiculos = new double[carroMotos.size()][2];
        for (int i = 0; i < carroMotos.size(); i++) {
            if (carga <= carroMotos.get(i).getCargaMaxima()) {
                veiculos[z][0] = carroMotos.get(i).getId();
                veiculos[z][1] = distancia / carroMotos.get(i).getVelocidadeMedia();
                z++;
            }
        }
        return ordenar(veiculos, 0);
    }

    public void setRendimentoGasolina(double rendimentoGasolina) {
        this.rendimentoGasolina = rendimentoGasolina;
    }

    public double getRendimentoAlcool() {
        return rendimentoAlcool;
    }

    public void setRendimentoAlcool(double rendimentoAlcool) {
        this.rendimentoAlcool = rendimentoAlcool;
    }

    public double getEsgotamentoGasolina() {
        return esgotamentoGasolina;
    }

    public void setEsgotamentoGasolina(double esgotamentoGasolina) {
        this.esgotamentoGasolina = esgotamentoGasolina;
    }

    public double getEsgotamentoAlcool() {
        return esgotamentoAlcool;
    }

    public void setEsgotamentoAlcool(double esgotamentoAlcool) {
        this.esgotamentoAlcool = esgotamentoAlcool;
    }
}
